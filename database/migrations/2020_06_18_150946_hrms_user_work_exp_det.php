<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HrmsUserWorkExpDet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlsrv_hrms')->create('hrms_user_work_exp_det', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('company_name');
            $table->text('company_address');
            $table->date('company_start_work');
            $table->date('company_end_work');
            $table->text('company_resign_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sqlsrv_hrms')->dropIfExists('hrms_user_work_exp_det');
    }
}
