<?php

namespace App\Mail\DMS;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $html)
    {
        $this->user = $user;
        $this->html = $html;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('DMS Approval Notification')->view('DMS.Email.approvalnotification', [ 
            'user' => $this->user,
            'html' => $this->html
        ]);
    }
}
