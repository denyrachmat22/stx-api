<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HrmsOccMstr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlsrv_hrms')->create('hrms_occ_mstr', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('occ_name');
            $table->string('occ_parent_id');
            $table->string('division_id');
            $table->integer('occ_emp_count');
            $table->string('domain_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sqlsrv_hrms')->dropIfExists('hrms_occ_mstr');
    }
}
