<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HrmsContentForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlsrv_hrms')->create('hrms_form_mstr', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_id');
            $table->string('form_type');
            $table->string('form_var');
            $table->string('form_label');
            $table->boolean('form_req');
            $table->string('form_username');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sqlsrv_hrms')->dropIfExists('hrms_form_mstr');
    }
}
